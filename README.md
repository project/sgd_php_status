# Site Guardian PHP Status module

Site Guardian PHP Status adds additional PHP information to the site Status Report.

It provides additional PHP environment information regarding the PHP installation on the server hosting the Drupal system that is not included in the standard Status report.

Many of these values are critical in avoiding or debugging issues and if you don't have access to your server then your only option is to look at the PHP Information available from a link in Status report and sift through it.

Values added are...

- PHP post max size
- PHP upload max filesize
- PHP realpath cache size
- PHP realpath cache TTL
- PHP max execution time
- PHP display errors
- PHP error reporting
- PHP log errors
- PHP error log
- PHP short open tag
- PHP max input vars

For a full description of the module, visit the
[project page](https://www.drupal.org/project/sgd_php_status).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/sgd_php_status).

## Table of contents

- Recommended modules
- Installation
- Configuration
- Maintainers

## Recommended modules

Site Guardian PHP Status is a companion module for the Site Guardian (API) module and as such will add to the information returned to a consumer of the Site Guardian (API).

See the Site Guardian [project page](https://www.drupal.org/project/site_guardian).

If used in conjunction with the Site Guardian API it returns additional information when a consumer of the Site Guardian (API) requests the sites information.

At present the module returns (in addition to the values listed above) a list of the PHP loaded extensions.

Although part of the Site Guardian ecosystem it provides functionality that is useful irrespective of whether the Site Guardian (API) is installed or not and does not require the Site Guardian (API) module to function.

## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will add information to the Site Status report as described above.

## Maintainers

- Andy Jones - [arcaic](https://www.drupal.org/u/arcaic)
